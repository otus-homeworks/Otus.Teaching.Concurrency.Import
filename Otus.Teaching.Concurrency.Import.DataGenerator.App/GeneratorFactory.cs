using Otus.Teaching.Concurrency.Import.Handler.Data;
using System;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;
using CsvDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.CsvGenerator;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileName, int dataCount)
        {
            return (Path.GetExtension(fileName)) switch
            {
                ".xml" => new XmlDataGenerator(fileName, dataCount),
                ".csv" => new CsvDataGenerator(fileName, dataCount),
                _ => throw new ArgumentException("�� ������� �� ���������� ����� ���������� ������������ ������"),
            };
        }
    }
}