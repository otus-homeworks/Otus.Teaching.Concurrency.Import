using Otus.Teaching.Concurrency.Import.Core.Dto;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    public class Customer
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public Customer()
        {
        }
        public Customer(CustomerAddUpdateRequest addupdateDto)
        {
            Id = addupdateDto.Id;
            FullName = addupdateDto.FullName;
            Email = addupdateDto.Email;
            Phone = addupdateDto.Phone;
        }

        public override string ToString()
        {
            return Id.ToString() + " " + FullName + " " + Email + " " + Phone;
        }
    }
}
