﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Dto
{
    public class CustomerResponse
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public CustomerResponse()
        {
        }

        public CustomerResponse(Customer customer)
        {
            FullName = customer.FullName;
            Email = customer.Email;
            Phone = customer.Phone;
        }
    }
}
