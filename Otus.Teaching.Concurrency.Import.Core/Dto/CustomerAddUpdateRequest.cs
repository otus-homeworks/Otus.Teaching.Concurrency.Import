﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Concurrency.Import.Core.Dto
{
    public class CustomerAddUpdateRequest
    {
        [Required]
        public int Id { get; set; }
        
        [Required]
        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}   FullName: {FullName}   Email: {Email}   Phone: {Phone}";
        }
    }
}
