﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.Core
{
    public class AppsSettings
    {
        public const string DataGeneratorExecutableFileName = "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";

        private readonly TypeOfFileGeneration _typeOfFileGeneration;
        private readonly string _dataFileName;
        private readonly int _dataCount;
        private readonly string _projectsPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName;

        private readonly DataLoadingMethod _dataLoadingMethod;
        private readonly byte _maxLoadRetryNum;
                 
        private readonly CustomersFileFormat _customersFileFormat;


        public TypeOfFileGeneration TypeOfFileGeneration => _typeOfFileGeneration;
        public string DataFileName => _dataFileName;
        public int DataCount => _dataCount;
        public string ProjectsPath => _projectsPath;

        public byte ThreadsCount;

        public DataLoadingMethod DataLoadingMethod => _dataLoadingMethod;
        public CustomersFileFormat CustomersFileFormat => _customersFileFormat;
        public byte MaxLoadRetryNum => _maxLoadRetryNum;


        public AppsSettings(IConfigurationRoot configuration)
        {
            if (!Enum.TryParse(configuration["typeOfFileGeneration"], out _typeOfFileGeneration))
            {
                Console.WriteLine($"Ошибка при считывании параметра 'тип запуска' для генерации файла с данными в файле AppsSettings.json {configuration["typeOfFileGeneration"]} \n Будет использован вызов метода.");
                _typeOfFileGeneration = TypeOfFileGeneration.ByMethodCall;
            }

            _dataFileName = configuration["dataFileName"];
            if (string.IsNullOrEmpty(_dataFileName))
            {
                _dataFileName = "customers";
                Console.WriteLine($"Ошибка при считывании параметра 'имя файла' для генерации файла с данными в файле AppsSettings.json {configuration["dataFileName"]} \n Будет задано имя файла: {_dataFileName}");
            }

            _dataFileName = Path.Combine(_projectsPath, _dataFileName);

            if (!int.TryParse(configuration["dataCount"], out _dataCount))
            {
                _dataCount = 100;
                Console.WriteLine($"Ошибка при считывании параметра 'количество записей' для генерации файла с данными в файле LoaderSettings.json {configuration["dataCount"]} \n Будет задано число записей: {_dataCount}");
            }

            if (!byte.TryParse(configuration["threadsCount"], out ThreadsCount))
            {
                ThreadsCount = (byte)Environment.ProcessorCount;
                Console.WriteLine($"Ошибка при считывании параметра 'количество потоков' для обработки данных в файле AppsSettings.json {configuration["threadsCount"]} \n Задано число потоков: {ThreadsCount}.");
            }

            if (!Enum.TryParse(configuration["dataLoadingMethod"], out _dataLoadingMethod))
            {
                _dataLoadingMethod = DataLoadingMethod.ByThreadPool;
                Console.WriteLine($"Ошибка при считывании параметра 'тип Loading-а' в файле AppsSettings.json {configuration["dataLoadingMethod"]} \n Будет использован {_dataLoadingMethod}.");
            }

            if (!Enum.TryParse(configuration["customersFileFormat"], out _customersFileFormat))
            {
                _customersFileFormat = CustomersFileFormat.Xml;
                Console.WriteLine($"Ошибка при считывании параметра 'тип файла с данными Customers' в файле AppsSettings.json {configuration["сustomersFileFormat"]} \n Будет использован {_customersFileFormat}.");
            }

            if (!byte.TryParse(configuration["threadsCount"], out _maxLoadRetryNum))
            {
                _maxLoadRetryNum = 5;
                Console.WriteLine($"Ошибка при считывании параметра 'количество Retry в LoadData' в файле AppsSettings.json {configuration["threadsCount"]} \n Будет задано кол-во попыток: 5{_maxLoadRetryNum}.");
            }
        }

        public string GetDataFileNameWithExt()
        {
            return DataFileName + "." + CustomersFileFormat.ToString().ToLower();
        }
    }
}
