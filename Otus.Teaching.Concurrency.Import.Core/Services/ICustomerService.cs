﻿using Otus.Teaching.Concurrency.Import.Core.Dto;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Services
{
    public interface ICustomerService
    {
        Task<CustomerResponse> GetByIdAsync(int id);
        bool Add(CustomerAddUpdateRequest request);
        //Task UpdateAsync(int id, CustomerAddUpdateRequest request);
        //Task DeleteAsync(int id);
    }
}
