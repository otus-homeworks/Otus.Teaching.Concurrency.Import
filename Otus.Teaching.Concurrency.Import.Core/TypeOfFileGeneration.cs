﻿namespace Otus.Teaching.Concurrency.Import.Core
{
    public enum TypeOfFileGeneration : byte
    {
        ByCreatingProcess,
        ByMethodCall
    }
}
