﻿namespace Otus.Teaching.Concurrency.Import.Core
{
    public enum DataLoadingMethod : byte
    {
        FakeDataLoader,
        BySingleThread,
        ByThreadsOnly,
        ByThreadPool
    }
}
