﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core
{
    public enum CustomersFileFormat : byte
    {
        Xml,
        Csv
    }
}
