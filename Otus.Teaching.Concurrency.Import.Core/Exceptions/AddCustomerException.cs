﻿using Otus.Teaching.Concurrency.Import.Core.Dto;
using System;

namespace Otus.Teaching.Concurrency.Import.Core.Exceptions
{
    public class AddCustomerException : Exception
    {
        public AddCustomerException()
        {
        }

        public AddCustomerException(CustomerAddUpdateRequest request)
            : base($"Customer {request} not added")
        {
        }
    }
}
