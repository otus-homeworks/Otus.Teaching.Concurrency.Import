using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {        
        bool AddCustomer(Customer customer, byte retryNum);
        bool AddCustomers(IEnumerable<Customer> customers, byte retryNum);

        Task<Customer> GetByIdAsync(int id);
    }
}