﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.Core.Dto;
using Otus.Teaching.Concurrency.Import.Core.Services;
using System.Threading.Tasks;

namespace WebHost.Controllers
{
#pragma warning disable 1591

    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]

    public class CustomerController : ControllerBase
    {
        private readonly ILogger<CustomerController> _logger;
        private readonly ICustomerService _customerService;

        public CustomerController(ILogger<CustomerController> logger, ICustomerService customerService)
        {
            _logger = logger;
            _customerService = customerService;
        }

        // GET api/<CustomerController>/5
        /// <summary>
        /// Get customer by id
        /// </summary>
        /// <param name="id">customer id</param>
        /// <returns>customer</returns>        
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetByIdAsync(int id)
        {
            CustomerResponse response = await _customerService.GetByIdAsync(id);
            if (response == null)
                return NotFound();

            return Ok(response);
        }

        // POST api/<CustomerController>
        /// <summary>
        /// Add customer
        /// </summary>
        /// <param name="customerAddRequest">customer</param>
        /// <returns>result</returns>        
        [HttpPost()]
        public async Task<ActionResult<int>> AddCustomerAsync([FromBody] CustomerAddUpdateRequest customerAddRequest)
        {
            CustomerResponse response = await _customerService.GetByIdAsync(customerAddRequest.Id);

            if (response != null)
                return Conflict();

            if (_customerService.Add(customerAddRequest))
                return Ok();
            else
                return StatusCode(500);
        }

    }
#pragma warning restore 1591
}
