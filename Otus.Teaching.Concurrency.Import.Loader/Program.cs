﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                                        .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                        .AddJsonFile("AppsSettings.json", optional: false)
                                        .Build();

            var appsSettings = new AppsSettings(configuration);

            GenerateCustomersDataFile(appsSettings);

            IDataParser<List<Customer>> parser = ParserFactory.GetParser(appsSettings);
            var customersList = parser.Parse();

            using var DBcontext = new CustomersDataContext();
            var repository = new CustomerRepository();
            repository.ClearAllData();

            var loader = LoaderCreator.GetLoader(appsSettings, customersList, repository);
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            loader.LoadData();

            if (repository.GetCount() == appsSettings.DataCount)
                Console.WriteLine($" В БД загружены данные {appsSettings.DataCount} пользователей");    // DataCount==1,2 не учитываю.
            else
                Console.WriteLine($"!!! В БД {repository.GetCount()} записей customer, а должно быть {appsSettings.DataCount} !!!???");

            stopWatch.Stop();            
            Console.WriteLine("Время за которое обработан файл: {0:00}:{1:00}.{2:000}\nНажмите Enter для завершения.", stopWatch.Elapsed.Minutes, stopWatch.Elapsed.Seconds,
                        stopWatch.Elapsed.Milliseconds);
           
            Console.ReadLine();
        }

        private static void GenerateCustomersDataFile(AppsSettings appsSettings)
        {
            Console.WriteLine($"Data generator type: {appsSettings.TypeOfFileGeneration}");
            switch (appsSettings.TypeOfFileGeneration)
            {
                case TypeOfFileGeneration.ByCreatingProcess:

                    string fileNameForRunFromDebug = Path.Combine(appsSettings.ProjectsPath, "Otus.Teaching.Concurrency.Import.DataGenerator.App\\bin\\Debug\\netcoreapp3.1", AppsSettings.DataGeneratorExecutableFileName);
                    string fileNameForRunFromRelease = Path.Combine(appsSettings.ProjectsPath, "Otus.Teaching.Concurrency.Import.DataGenerator.App\\bin\\Release\\netcoreapp3.1", AppsSettings.DataGeneratorExecutableFileName);

                    var procInfo = new ProcessStartInfo();
                    // Имя файла c расширением (расширение в зависимости от типа заданного в настройках).
                    procInfo.Arguments = appsSettings.GetDataFileNameWithExt() + " " + appsSettings.DataCount.ToString();
                    if (File.Exists(fileNameForRunFromRelease))
                        procInfo.FileName = fileNameForRunFromRelease;
                    else
                        if (File.Exists(fileNameForRunFromDebug))
                        procInfo.FileName = fileNameForRunFromDebug;
                    else
                    {
                        Console.WriteLine($"Не обнаружен ни один из файлов для запуска \n{fileNameForRunFromRelease}\n{fileNameForRunFromDebug}");
                        procInfo.FileName = "";
                    }

                    if (procInfo.FileName != "")
                    {
                        var process = Process.Start(procInfo);
                        Console.WriteLine($"Data generator started with process Id {process.Id}...");
                        process.WaitForExit();
                        Console.WriteLine("Data generator finished working");
                    };
                    break;

                case TypeOfFileGeneration.ByMethodCall:
                    string dataFileName = appsSettings.GetDataFileNameWithExt();
                    Console.WriteLine($"Generating {dataFileName} file with Customers data...");
                    GenerateCustomersDataFile(dataFileName, appsSettings.DataCount);
                    Console.WriteLine($"Generated customers data in {dataFileName}...");
                    break;
            }
        }

        static void GenerateCustomersDataFile(string fileName, int dataCount)
        {
            var xmlGenerator = new XmlGenerator(fileName, dataCount);
            xmlGenerator.Generate();
        }
    }
}