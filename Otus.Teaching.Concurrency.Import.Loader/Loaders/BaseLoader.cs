﻿using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public abstract class BaseLoader : IDataLoader
    {
        protected List<Customer> _customersList;
        protected byte _threadsNum;
        protected int _workingThreadsNum;
        protected ICustomerRepository _repository;
        protected byte _maxRetryNum;
        protected int _customersNumForThread;

        public BaseLoader(List<Customer> customersList, AppsSettings appsSettings, ICustomerRepository repository)
        {
            _customersList = customersList;
            _repository = repository;
            _threadsNum = appsSettings.ThreadsCount;
            _maxRetryNum = appsSettings.MaxLoadRetryNum;
            _customersNumForThread = _customersList.Count / _threadsNum;
        }

        public abstract void LoadData();

        protected void LoadCustomers(object thrdNun)
        {
            var (beginningIndexForThread, upToIndexForThread) = GetBeginAndFinishIndexesForThreadNum((byte)thrdNun);

            var customersChunk = _customersList.Skip(beginningIndexForThread).Take(upToIndexForThread - beginningIndexForThread);

            if (!_repository.AddCustomers(customersChunk, _maxRetryNum))
                throw new InvalidOperationException($"AddCustomers -> false, thread Id: {Thread.CurrentThread.ManagedThreadId}");

            Interlocked.Decrement(ref _workingThreadsNum);
        }

        // для потока с номером threadN обработать данные customers c индексами [beginningIndexForThread,upToIndexForThread).
        protected (int beginningIndexForThread, int upToIndexForThread) GetBeginAndFinishIndexesForThreadNum(byte threadN)
        {
            if (threadN == _threadsNum - 1)
                return (threadN * _customersNumForThread, _customersList.Count);
            else
                return (threadN * _customersNumForThread, (threadN + 1) * _customersNumForThread);
        }
    }
}
