﻿using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    class LoaderCreator
    {
        static public IDataLoader GetLoader(AppsSettings appsSettings, List<Customer> customersList, ICustomerRepository repository)
        {
            switch (appsSettings.DataLoadingMethod)
            {
                case DataLoadingMethod.FakeDataLoader:
                    return new FakeDataLoader();

                case DataLoadingMethod.BySingleThread:
                    return new BySingleThreadLoader(customersList, appsSettings, repository);

                case DataLoadingMethod.ByThreadsOnly:
                    return new ByThreadsLoader(customersList, appsSettings, repository);

                case DataLoadingMethod.ByThreadPool:
                    return new ByThreadPoolLoader(customersList, appsSettings, repository);

                default:
                    throw new ArgumentException("В GetLoader передан неверный аргумент.");
            }
        }
    }
}

