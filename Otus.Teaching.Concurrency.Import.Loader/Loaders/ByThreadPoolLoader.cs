﻿using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Collections.Generic;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ByThreadPoolLoader : BaseLoader
    {
        public ByThreadPoolLoader(List<Customer> customersList, AppsSettings appsSettings, ICustomerRepository repository)
            : base(customersList, appsSettings, repository)
        {
        }

        public override void LoadData()
        {
            _workingThreadsNum = _threadsNum;
            for (byte thrN = 0; thrN < _threadsNum; thrN++)
                ThreadPool.QueueUserWorkItem(LoadCustomers, thrN);

            // Ждём завершения всех потоков
            while (_workingThreadsNum > 0)
                Thread.Sleep(0);
        }
    }
}
