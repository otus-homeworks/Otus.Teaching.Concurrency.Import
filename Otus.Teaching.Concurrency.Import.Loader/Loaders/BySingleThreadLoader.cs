﻿using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    // Благодаря однопоточной реализации Loader понял, 
    // что в TxtFileCutomerRepository не сохранял нормально, не из-за ошибок в реализации потоков.
    // А из-за того, что поначалу работал непосредственно с файлом.
    // Аналогично и для БД - реализация позволила понять необходимость создания/удаления контеста на поток
    public class BySingleThreadLoader : BaseLoader
    {
        public BySingleThreadLoader(List<Customer> customersList, AppsSettings appsSettings, ICustomerRepository repository)
            : base(customersList, appsSettings, repository)
        {
        }

        public override void LoadData()
        {
            foreach (Customer cstr in _customersList)
            {                
                if (!_repository.AddCustomer(cstr, _maxRetryNum))
                    throw new InvalidOperationException();
            }
        }
    }
}
