﻿using CsvHelper;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        private readonly string _fileName;

        public CsvParser(string fileName)
        {
            _fileName = fileName;
        }

        public List<Customer> Parse()
        {
            using var stream = File.Open(_fileName, FileMode.Open);
            using var sr = new StreamReader(stream);
            using var csv = new CsvReader(sr, CultureInfo.CurrentCulture);
            var customersList = csv.GetRecords<Customer>();
            return customersList.ToList();
        }
    }
}
