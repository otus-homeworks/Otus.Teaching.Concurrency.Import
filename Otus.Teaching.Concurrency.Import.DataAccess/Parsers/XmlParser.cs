﻿using Otus.Teaching.Concurrency.Import.Core.Dto;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private readonly string _fileName;

        public XmlParser(string fileName)
        {
            _fileName = fileName;
        }

        public List<Customer> Parse()
        {            
            var serializer = new XmlSerializer(typeof(CustomersList));
            try
            {
                using var fileStream = new FileStream(_fileName, FileMode.Open);
                var customersList = (CustomersList)serializer.Deserialize(fileStream);
                return customersList.Customers;
            }
            catch
            {
                return new List<Customer>();
            }
        }
    }
}