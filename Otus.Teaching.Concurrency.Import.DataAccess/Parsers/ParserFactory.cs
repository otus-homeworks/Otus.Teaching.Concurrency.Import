﻿using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public static class ParserFactory
    {
        public static IDataParser<List<Customer>> GetParser(AppsSettings appsSettings)
        {
            switch (appsSettings.CustomersFileFormat)
            {
                case CustomersFileFormat.Xml:
                    return new XmlParser(appsSettings.GetDataFileNameWithExt());

                case CustomersFileFormat.Csv:
                    return new CsvParser(appsSettings.GetDataFileNameWithExt());
            }

            throw new ArgumentException();
        }
    }
}
