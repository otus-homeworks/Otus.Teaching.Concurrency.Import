﻿using Otus.Teaching.Concurrency.Import.Core.Dto;
using Otus.Teaching.Concurrency.Import.Core.Exceptions;
using Otus.Teaching.Concurrency.Import.Core.Services;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task<CustomerResponse> GetByIdAsync(int id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return null;

            return new CustomerResponse(customer);
        }
                
        public bool Add(CustomerAddUpdateRequest request)
        {
            return _customerRepository.AddCustomer(new Customer(request), 5);
        }
    }
}
