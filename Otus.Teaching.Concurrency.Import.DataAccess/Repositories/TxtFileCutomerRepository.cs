﻿using Otus.Teaching.Concurrency.Import.Core.Exceptions;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    // Изначально предполагалось без ConcurrentBag, только запись в файл, используя lock и StreamWriter.
    // Но так не работало. Возможно потому, что дескриптор файла создавал в конструкторе и освобождал в Dispose    

    public class TxtFileCutomerRepository : ICustomerRepository
    {
        private string _fileName;

        public ConcurrentBag<Customer> customersList = new ConcurrentBag<Customer>();

        public TxtFileCutomerRepository(string fileName)
        {
            _fileName = fileName;
        }

        public bool AddCustomer(Customer customer, byte retryNum)
        {
            try
            {
                customersList.Add(customer);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddCustomers(IEnumerable<Customer> customers, byte retryNum)
        {
            try
            {
                foreach (Customer cstm in customers)
                    customersList.Add(cstm);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<Customer> GetByIdAsync(int id)
        {
            if (id < customersList.Count)
                return customersList.ToArray()[id];
            else
                throw new CustomerNotFoundException(id);
        }

        public void WriteToFile()
        {
            using var sw = new StreamWriter(_fileName);
            foreach (Customer cstm in customersList)
                sw.WriteLine(cstm);

            sw.Close();
        }
    }
}
