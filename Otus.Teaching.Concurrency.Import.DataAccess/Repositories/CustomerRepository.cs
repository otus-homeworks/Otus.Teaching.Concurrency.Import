using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Exceptions;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {              
        public bool AddCustomer(Customer customer, byte retryNum)
        {
            int attemptNumber = 0;
            var rnd = new Random();

            while (attemptNumber < retryNum)
            {
                try
                {                    
                    Task.Run(() => TryAddCustomerAndSaveChangesAsync(customer)).Wait();
                    return true;
                }
                catch (Exception)
                {
                    attemptNumber++;
                    Thread.Sleep(rnd.Next(100));
                }
            }
            return false;
        }


        public bool AddCustomers(IEnumerable<Customer> customers, byte retryNum)
        {
            //Add customer to data source
            int attemptNumber = 0;
            var rnd = new Random();

            while (attemptNumber < retryNum)
            {
                try
                {
                    //TryAddCustomersAndSaveChanges(customers);
                    Task.Run(() => TryAddCustomersAndSaveChangesAsync(customers)).Wait();
                    return true;
                }
                catch (Exception)
                {
                    attemptNumber++;
                    Thread.Sleep(rnd.Next(100));
                }
            }
            return false;
        }

        public async Task<Customer> GetByIdAsync(int id)
        {
            using var _context = new CustomersDataContext();
            var customer = await _context.Customers.FindAsync(id);
            return customer;
        }

        private int TryAddCustomerAndSaveChanges(Customer customer)
        {
            using var _context = new CustomersDataContext();
            
            _context.Customers.Add(customer);
            _context.SaveChanges();
            return customer.Id;
        }

        private async Task TryAddCustomerAndSaveChangesAsync(Customer customer)
        {
            using var _context = new CustomersDataContext();
            await _context.Customers.AddAsync(customer);
            await _context.SaveChangesAsync();            
        }

        private void TryAddCustomersAndSaveChanges(IEnumerable<Customer> customers)
        {
            using var _context = new CustomersDataContext();
            _context.Customers.AddRange(customers);
            _context.SaveChanges();
        }

        private async Task TryAddCustomersAndSaveChangesAsync(IEnumerable<Customer> customers)
        {
            using var _context = new CustomersDataContext();
            await _context.Customers.AddRangeAsync(customers);
            await _context.SaveChangesAsync();
        }


        public void ClearAllData()
        {
            using var _context = new CustomersDataContext();
            _context.Database.ExecuteSqlRaw("TRUNCATE TABLE \"Customers\"");
            
            // More correct, but also more slowly
            //_context.Customers.RemoveRange(_context.Customers);
            //_context.SaveChanges();
        }

        public int GetCount()
        {
            using var _context = new CustomersDataContext();
            return _context.Customers.Count();
        }
    }
}