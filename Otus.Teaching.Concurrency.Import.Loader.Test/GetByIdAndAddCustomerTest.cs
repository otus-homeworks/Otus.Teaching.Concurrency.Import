﻿using Otus.Teaching.Concurrency.Import.Core.Dto;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.Concurrency.Import.Loader.Tests
{
    public class GetByIdAndAddCustomerTest
    {
        [Fact]
        public void GetByIdAndAddCustomerTestForRepository()
        {
            // Arrange
            var repository = new CustomerRepository();
            repository.ClearAllData();
            var addTestCustomerDto = new CustomerAddUpdateRequest()
            {
                Id = 5,
                FullName = "testUser",
                Email = "emailForTestUser",
                Phone = "8927..."
            };

            // Act
            var customerForEmptyDB = Task.Run(() => repository.GetByIdAsync(addTestCustomerDto.Id)).Result;
            var addResult = repository.AddCustomer(new Customer(addTestCustomerDto), 5);
            var customerAfterAdd = Task.Run(() => repository.GetByIdAsync(addTestCustomerDto.Id)).Result;

            // Assert
            Assert.Null(customerForEmptyDB);
            Assert.True(addResult);
            Assert.NotNull(customerAfterAdd);
            Assert.Equal(addTestCustomerDto.Id, customerAfterAdd.Id);
            Assert.Equal(addTestCustomerDto.FullName, customerAfterAdd.FullName);
            Assert.Equal(addTestCustomerDto.Email, customerAfterAdd.Email);
            Assert.Equal(addTestCustomerDto.Phone, customerAfterAdd.Phone);
        }
    }
}
