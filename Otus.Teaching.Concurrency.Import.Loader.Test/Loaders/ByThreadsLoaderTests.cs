﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace Otus.Teaching.Concurrency.Import.Loader.Test.Loaders
{
    public class ByThreadsLoaderTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(2)]
        public void ByThreadsLoaderTest(byte loaderType)
        {
            // Arrange
            string fileName = "CustomersForTest.txt";
            var txtFileRepository = new TxtFileCutomerRepository(fileName);

            var configuration = new ConfigurationBuilder()
                                                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                                    .AddJsonFile("AppsSettingsForTest.json", optional: false).Build();

            var appsSettings = new AppsSettings(configuration);
            var customersList = RandomCustomerGenerator.Generate(appsSettings.DataCount);

            IDataLoader loader;
            switch (loaderType)
            {
                case 0:
                    loader = new BySingleThreadLoader(customersList, appsSettings, txtFileRepository);
                    break;

                case 1:
                    loader = new ByThreadsLoader(customersList, appsSettings, txtFileRepository);
                    break;

                default:
                    loader = new ByThreadPoolLoader(customersList, appsSettings, txtFileRepository);
                    break;
            }

            // Act
            loader.LoadData();
            txtFileRepository.WriteToFile();

            // Assert
            Assert.Equal(1111, appsSettings.DataCount);
            Assert.Equal(appsSettings.DataCount, customersList.Count);
            Assert.Equal(appsSettings.DataCount, txtFileRepository.customersList.Count);
            var lines = File.ReadAllLines(fileName);
            Assert.NotNull(lines);
            Assert.Equal(appsSettings.DataCount, lines.Length);
            Assert.True(CompareReadedWhithSource.CompareCustomersListWithLinesFromFile(customersList, lines));
            File.Delete(fileName);
        }
    }

    public class CompareReadedWhithSource
    {
        public static bool CompareCustomersListWithLinesFromFile(List<Customer> customersList, string[] lines)
        {
            foreach (Customer cstm in customersList)
            {
                string cstmStr = cstm.ToString();
                for (int i = 0; i < lines.Length; i++)
                    if (lines[i] == cstmStr)
                        goto viewNextCustomer;

                return false;

                viewNextCustomer:;
            }

            return true;
        }
    }
}
