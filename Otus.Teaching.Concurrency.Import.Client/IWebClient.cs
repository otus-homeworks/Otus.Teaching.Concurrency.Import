﻿using Otus.Teaching.Concurrency.Import.Core.Dto;
using System.Net;

namespace Otus.Teaching.Concurrency.Import.Client
{
    public interface IWebClient
    {
        public HttpStatusCode AddCustomer(CustomerAddUpdateRequest addRequest);
        public CustomerResponse GetById(int Id, out HttpStatusCode statusCode);
    }
}
