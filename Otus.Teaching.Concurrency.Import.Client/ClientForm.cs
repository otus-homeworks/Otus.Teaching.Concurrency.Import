﻿using Otus.Teaching.Concurrency.Import.Core.Dto;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Net;
using System.Windows.Forms;

namespace Otus.Teaching.Concurrency.Import.Client
{
    public partial class ClientForm : Form
    {
        private readonly IWebClient _webClient = new WebClient();

        public ClientForm()
        {
            InitializeComponent();
        }

        static private bool IsTextInControlValidInt(Control control)
        {
            if (!int.TryParse(control.Text, out _))
            {
                MessageBox.Show("Ошибка при вводе.");
                control.Focus();
                return false;
            }

            return true;
        }

        private CustomerAddUpdateRequest GetCustomerAddUpdateRequestFromInputFields()
        {
            return new CustomerAddUpdateRequest()
            {
                Id = Convert.ToInt32(tbxId.Text),
                FullName = tbxFullName.Text,
                Email = tbxEmail.Text,
                Phone = tbxPhone.Text
            };
        }

        private void btnGetById_Click(object sender, EventArgs e)
        {
            if (IsTextInControlValidInt(tbId))
            {
                int Id = Convert.ToInt32(tbId.Text);

                try
                {
                    DisableEnableAll(false);

                    var customerResponse = _webClient.GetById(Id, out HttpStatusCode statusCode);

                    ShowCustomerInfo(Id, customerResponse);
                    lbStatusCodeForGetById.Text = "код ответа: " + statusCode.ToString();
                }
                catch (Exception ex)
                {
                    ShowCustomerInfo(0, null);
                    lbStatusCodeForGetById.Text = "код ответа: ошибка";
                    MessageBox.Show(ex.Message);
                }

                DisableEnableAll(true);
            }
        }

        private void DisableEnableAll(bool kind)
        {
            tbId.Enabled = kind;
            btnGetById.Enabled = kind;
            btnInsWithSpecifiedData.Enabled = kind;
            btnInsWithRandomData.Enabled = kind;

            tbxId.Enabled = kind;
            tbxFullName.Enabled = kind;
            tbxEmail.Enabled = kind;
            tbxPhone.Enabled = kind;
        }

        private void ShowCustomerInfo(int id, CustomerResponse customerResponse)
        {
            if (customerResponse != null)
            {
                gbCustomerInfo.Text = "Customer c Id:" + id;
                lbFullName.Text = customerResponse.FullName;
                lbEmail.Text = customerResponse.Email;
                lbPhone.Text = customerResponse.Phone;
                gbCustomerInfo.Visible = true;
            }
            else
                gbCustomerInfo.Visible = false;
        }

        private void btnInsWithSpecifiedData_Click(object sender, EventArgs e)
        {
            if (IsTextInControlValidInt(tbxId))
            {
                try
                {
                    DisableEnableAll(false);

                    var statusCode = _webClient.AddCustomer(GetCustomerAddUpdateRequestFromInputFields());

                    lbStatusCodeForAddCustomer.Text = "код ответа: " + statusCode.ToString();
                }
                catch (Exception ex)
                {
                    lbStatusCodeForAddCustomer.Text = "код ответа: ошибка";
                    MessageBox.Show(ex.Message);
                }

                DisableEnableAll(true);
            }
        }

        private void btnInsWithRandomData_Click(object sender, EventArgs e)
        {
            var customer = RandomCustomerGenerator.Generate(1)[0];

            customer.Id = new Random().Next(100);

            ShowCustomerInformationInInputFields(customer);

            try
            {
                DisableEnableAll(false);

                var statusCode = _webClient.AddCustomer(new CustomerAddUpdateRequest()
                {
                    Id = customer.Id,
                    FullName = customer.FullName,
                    Email = customer.Email,
                    Phone = customer.Phone
                });

                lbStatusCodeForAddCustomer.Text = "код ответа: " + statusCode.ToString();
            }
            catch (Exception ex)
            {
                lbStatusCodeForAddCustomer.Text = "код ответа: ошибка";
                MessageBox.Show(ex.Message);
            }

            DisableEnableAll(true);
        }

        private void ShowCustomerInformationInInputFields(Customer customer)
        {
            tbxId.Text = customer.Id.ToString();
            tbxFullName.Text = customer.FullName;
            tbxEmail.Text = customer.Email;
            tbxPhone.Text = customer.Phone;
        }
    }
}
