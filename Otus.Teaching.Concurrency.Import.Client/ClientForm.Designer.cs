﻿
namespace Otus.Teaching.Concurrency.Import.Client
{
    partial class ClientForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientForm));
            this.lbGetById = new System.Windows.Forms.Label();
            this.lbInsWithSpecifiedData = new System.Windows.Forms.Label();
            this.lbInsWithRandomData = new System.Windows.Forms.Label();
            this.gbCustomerInfo = new System.Windows.Forms.GroupBox();
            this.lbPhone = new System.Windows.Forms.Label();
            this.lbEmail = new System.Windows.Forms.Label();
            this.lbFullName = new System.Windows.Forms.Label();
            this.tbId = new System.Windows.Forms.TextBox();
            this.btnGetById = new System.Windows.Forms.Button();
            this.btnInsWithSpecifiedData = new System.Windows.Forms.Button();
            this.btnInsWithRandomData = new System.Windows.Forms.Button();
            this.lbStatusCodeForGetById = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxPhone = new System.Windows.Forms.TextBox();
            this.tbxEmail = new System.Windows.Forms.TextBox();
            this.tbxFullName = new System.Windows.Forms.TextBox();
            this.tbxId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbStatusCodeForAddCustomer = new System.Windows.Forms.Label();
            this.gbCustomerInfo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbGetById
            // 
            this.lbGetById.AutoSize = true;
            this.lbGetById.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbGetById.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.lbGetById.ForeColor = System.Drawing.Color.Blue;
            this.lbGetById.Location = new System.Drawing.Point(12, 21);
            this.lbGetById.Name = "lbGetById";
            this.lbGetById.Size = new System.Drawing.Size(177, 19);
            this.lbGetById.TabIndex = 0;
            this.lbGetById.Text = "Выдать customer-а по Id";
            // 
            // lbInsWithSpecifiedData
            // 
            this.lbInsWithSpecifiedData.AutoSize = true;
            this.lbInsWithSpecifiedData.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbInsWithSpecifiedData.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.lbInsWithSpecifiedData.ForeColor = System.Drawing.Color.Blue;
            this.lbInsWithSpecifiedData.Location = new System.Drawing.Point(12, 226);
            this.lbInsWithSpecifiedData.Name = "lbInsWithSpecifiedData";
            this.lbInsWithSpecifiedData.Size = new System.Drawing.Size(310, 19);
            this.lbInsWithSpecifiedData.TabIndex = 1;
            this.lbInsWithSpecifiedData.Text = "Вставить customer-а c заданными данными";
            // 
            // lbInsWithRandomData
            // 
            this.lbInsWithRandomData.AutoSize = true;
            this.lbInsWithRandomData.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbInsWithRandomData.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.lbInsWithRandomData.ForeColor = System.Drawing.Color.Blue;
            this.lbInsWithRandomData.Location = new System.Drawing.Point(12, 262);
            this.lbInsWithRandomData.Name = "lbInsWithRandomData";
            this.lbInsWithRandomData.Size = new System.Drawing.Size(317, 19);
            this.lbInsWithRandomData.TabIndex = 2;
            this.lbInsWithRandomData.Text = "Вставить customer-а c случайными данными";
            // 
            // gbCustomerInfo
            // 
            this.gbCustomerInfo.BackColor = System.Drawing.Color.Cornsilk;
            this.gbCustomerInfo.Controls.Add(this.lbPhone);
            this.gbCustomerInfo.Controls.Add(this.lbEmail);
            this.gbCustomerInfo.Controls.Add(this.lbFullName);
            this.gbCustomerInfo.Location = new System.Drawing.Point(21, 84);
            this.gbCustomerInfo.Name = "gbCustomerInfo";
            this.gbCustomerInfo.Size = new System.Drawing.Size(281, 97);
            this.gbCustomerInfo.TabIndex = 3;
            this.gbCustomerInfo.TabStop = false;
            this.gbCustomerInfo.Text = "groupBox1";
            this.gbCustomerInfo.Visible = false;
            // 
            // lbPhone
            // 
            this.lbPhone.AutoSize = true;
            this.lbPhone.Location = new System.Drawing.Point(10, 68);
            this.lbPhone.Name = "lbPhone";
            this.lbPhone.Size = new System.Drawing.Size(38, 15);
            this.lbPhone.TabIndex = 3;
            this.lbPhone.Text = "label4";
            // 
            // lbEmail
            // 
            this.lbEmail.AutoSize = true;
            this.lbEmail.Location = new System.Drawing.Point(10, 45);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(38, 15);
            this.lbEmail.TabIndex = 2;
            this.lbEmail.Text = "label3";
            // 
            // lbFullName
            // 
            this.lbFullName.AutoSize = true;
            this.lbFullName.Location = new System.Drawing.Point(10, 23);
            this.lbFullName.Name = "lbFullName";
            this.lbFullName.Size = new System.Drawing.Size(38, 15);
            this.lbFullName.TabIndex = 1;
            this.lbFullName.Text = "label2";
            // 
            // tbId
            // 
            this.tbId.BackColor = System.Drawing.SystemColors.Info;
            this.tbId.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tbId.Location = new System.Drawing.Point(195, 19);
            this.tbId.Name = "tbId";
            this.tbId.Size = new System.Drawing.Size(68, 25);
            this.tbId.TabIndex = 4;
            this.tbId.TabStop = false;
            this.tbId.Text = "1";
            this.tbId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnGetById
            // 
            this.btnGetById.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGetById.BackgroundImage")));
            this.btnGetById.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGetById.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGetById.Location = new System.Drawing.Point(279, 20);
            this.btnGetById.Name = "btnGetById";
            this.btnGetById.Size = new System.Drawing.Size(23, 23);
            this.btnGetById.TabIndex = 8;
            this.btnGetById.UseVisualStyleBackColor = true;
            this.btnGetById.Click += new System.EventHandler(this.btnGetById_Click);
            // 
            // btnInsWithSpecifiedData
            // 
            this.btnInsWithSpecifiedData.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnInsWithSpecifiedData.BackgroundImage")));
            this.btnInsWithSpecifiedData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnInsWithSpecifiedData.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnInsWithSpecifiedData.Location = new System.Drawing.Point(345, 226);
            this.btnInsWithSpecifiedData.Name = "btnInsWithSpecifiedData";
            this.btnInsWithSpecifiedData.Size = new System.Drawing.Size(23, 23);
            this.btnInsWithSpecifiedData.TabIndex = 9;
            this.btnInsWithSpecifiedData.UseVisualStyleBackColor = true;
            this.btnInsWithSpecifiedData.Click += new System.EventHandler(this.btnInsWithSpecifiedData_Click);
            // 
            // btnInsWithRandomData
            // 
            this.btnInsWithRandomData.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnInsWithRandomData.BackgroundImage")));
            this.btnInsWithRandomData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnInsWithRandomData.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnInsWithRandomData.Location = new System.Drawing.Point(345, 258);
            this.btnInsWithRandomData.Name = "btnInsWithRandomData";
            this.btnInsWithRandomData.Size = new System.Drawing.Size(23, 23);
            this.btnInsWithRandomData.TabIndex = 10;
            this.btnInsWithRandomData.UseVisualStyleBackColor = true;
            this.btnInsWithRandomData.Click += new System.EventHandler(this.btnInsWithRandomData_Click);
            // 
            // lbStatusCodeForGetById
            // 
            this.lbStatusCodeForGetById.AutoSize = true;
            this.lbStatusCodeForGetById.BackColor = System.Drawing.Color.Cornsilk;
            this.lbStatusCodeForGetById.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbStatusCodeForGetById.Location = new System.Drawing.Point(21, 53);
            this.lbStatusCodeForGetById.Name = "lbStatusCodeForGetById";
            this.lbStatusCodeForGetById.Size = new System.Drawing.Size(0, 19);
            this.lbStatusCodeForGetById.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Cornsilk;
            this.groupBox1.Controls.Add(this.tbxPhone);
            this.groupBox1.Controls.Add(this.tbxEmail);
            this.groupBox1.Controls.Add(this.tbxFullName);
            this.groupBox1.Controls.Add(this.tbxId);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 302);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(290, 133);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Customers data";
            // 
            // tbxPhone
            // 
            this.tbxPhone.BackColor = System.Drawing.SystemColors.Info;
            this.tbxPhone.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tbxPhone.Location = new System.Drawing.Point(57, 101);
            this.tbxPhone.Name = "tbxPhone";
            this.tbxPhone.Size = new System.Drawing.Size(217, 23);
            this.tbxPhone.TabIndex = 8;
            this.tbxPhone.Text = "8927..........";
            // 
            // tbxEmail
            // 
            this.tbxEmail.BackColor = System.Drawing.SystemColors.Info;
            this.tbxEmail.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tbxEmail.Location = new System.Drawing.Point(55, 73);
            this.tbxEmail.Name = "tbxEmail";
            this.tbxEmail.Size = new System.Drawing.Size(219, 23);
            this.tbxEmail.TabIndex = 7;
            this.tbxEmail.Text = "vasia@gmail.ru";
            // 
            // tbxFullName
            // 
            this.tbxFullName.BackColor = System.Drawing.SystemColors.Info;
            this.tbxFullName.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tbxFullName.Location = new System.Drawing.Point(74, 44);
            this.tbxFullName.Name = "tbxFullName";
            this.tbxFullName.Size = new System.Drawing.Size(200, 23);
            this.tbxFullName.TabIndex = 6;
            this.tbxFullName.Text = "Иванов Василий";
            // 
            // tbxId
            // 
            this.tbxId.BackColor = System.Drawing.SystemColors.Info;
            this.tbxId.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tbxId.Location = new System.Drawing.Point(55, 18);
            this.tbxId.Name = "tbxId";
            this.tbxId.Size = new System.Drawing.Size(46, 23);
            this.tbxId.TabIndex = 5;
            this.tbxId.Text = "101";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Id";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Phone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "FullName";
            // 
            // lbStatusCodeForAddCustomer
            // 
            this.lbStatusCodeForAddCustomer.AutoSize = true;
            this.lbStatusCodeForAddCustomer.BackColor = System.Drawing.Color.Cornsilk;
            this.lbStatusCodeForAddCustomer.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbStatusCodeForAddCustomer.Location = new System.Drawing.Point(12, 438);
            this.lbStatusCodeForAddCustomer.Name = "lbStatusCodeForAddCustomer";
            this.lbStatusCodeForAddCustomer.Size = new System.Drawing.Size(0, 19);
            this.lbStatusCodeForAddCustomer.TabIndex = 13;
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(424, 491);
            this.Controls.Add(this.lbStatusCodeForAddCustomer);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbStatusCodeForGetById);
            this.Controls.Add(this.btnInsWithRandomData);
            this.Controls.Add(this.btnInsWithSpecifiedData);
            this.Controls.Add(this.btnGetById);
            this.Controls.Add(this.tbId);
            this.Controls.Add(this.gbCustomerInfo);
            this.Controls.Add(this.lbInsWithRandomData);
            this.Controls.Add(this.lbInsWithSpecifiedData);
            this.Controls.Add(this.lbGetById);
            this.Name = "ClientForm";
            this.Text = "Customers client";
            this.gbCustomerInfo.ResumeLayout(false);
            this.gbCustomerInfo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbGetById;
        private System.Windows.Forms.Label lbInsWithSpecifiedData;
        private System.Windows.Forms.Label lbInsWithRandomData;
        private System.Windows.Forms.GroupBox gbCustomerInfo;
        private System.Windows.Forms.Label lbPhone;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.Label lbFullName;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.Button btnGetById;
        private System.Windows.Forms.Button btnInsWithSpecifiedData;
        private System.Windows.Forms.Button btnInsWithRandomData;
        private System.Windows.Forms.Label lbStatusCodeForGetById;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxPhone;
        private System.Windows.Forms.TextBox tbxEmail;
        private System.Windows.Forms.TextBox tbxFullName;
        private System.Windows.Forms.TextBox tbxId;
        private System.Windows.Forms.Label lbStatusCodeForAddCustomer;
    }
}

