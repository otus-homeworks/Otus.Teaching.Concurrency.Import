﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Core.Dto;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Client
{
    public class WebClient : IWebClient
    {
        private static readonly string _api_Customer = "https://localhost:5001/Customer";
        public CustomerResponse GetById(int Id, out HttpStatusCode statusCode)
        {
            var strUri = $@"{_api_Customer}/{Id}";
            var httpClient = new HttpClient();

            var response = httpClient.GetAsync(strUri).GetAwaiter().GetResult();

            statusCode = response.StatusCode;

            if (response.IsSuccessStatusCode)
            {
                var customerStr = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<CustomerResponse>(customerStr);
            }
            else
                return null;
        }

        public HttpStatusCode AddCustomer(CustomerAddUpdateRequest addRequest)
        {
            var httpClient = new HttpClient();
            var customerJson = JsonConvert.SerializeObject(addRequest);
            var httpPostConent = new StringContent(customerJson, Encoding.UTF8, "application/json");
            var response = httpClient.PostAsync(_api_Customer, httpPostConent).GetAwaiter().GetResult();
            return response.StatusCode;
        }
    }
}
